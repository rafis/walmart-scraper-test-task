<?php

require(__DIR__.'/vendor/autoload.php');

$pathname = '/ip/Danskin-Now-Women-s-Knit-Slip-on-Shoe/51630300';
//$pathname = '/ip/Apple-iPhone-7-128GB-GSM-4G-LTE-Quad-Core-Smartphone-with-12MP-Camera-Unlocked/55174745';
$fields = ['title', 'variants', 'offers', 'shipping:10001'];
$result = [];

$productScraper = new \App\Scrapers\WalmartCom($pathname);

foreach($fields as $field) {
    
    try {
            
        $result[$field] = $productScraper->scrape($field);
            
    } catch(\Exception $e) {

        file_put_contents('php://stderr', $e->getMessage().' '.$e->getTraceAsString().PHP_EOL, FILE_APPEND);

    }
    
}

echo(json_encode($result, JSON_PRETTY_PRINT).PHP_EOL);
