<?php

namespace App;

/**
 * Common functions used in all scrapers
 */
trait ScrapingHelper
{
    
    /**
     * Replace &nbsp; character with space
     * 
     * @param string $str
     * @return string
     */
    protected function convertNbspToSpace($str)
    {
        return str_replace("\xC2\xA0", ' ', $str);
    }
    
    /**
     * Convert price from any currency to USD
     * 
     * @param double $price
     * @param string $currency
     * @return double
     * @throws \InvalidArgumentException
     */
    protected function convertToUSD($price, $currency)
    {
        if ('USD' != $currency) {
            throw new \InvalidArgumentException('Currency "'.$currency.'" is not supported yet.');
        }
        
        return 1.0 * $price;
    }
    
    /**
     * Convert miles to meters
     * 
     * @param double|int $distanceInMiles
     * @return double
     */
    protected function convertMilesToMeters($distanceInMiles)
    {
        if ('double' != gettype($distanceInMiles) && 'integer' != gettype($distanceInMiles)) {
            throw new \InvalidArgumentException('Invalid type of distance in miles argument value "'.gettype($distanceInMiles).'"');
        }
        
        return 1609.34 * $distanceInMiles;
    }
    
    /**
     * Calculate discount percent based on sale price and list price
     * 
     * @param double $salePrice
     * @param double $listPrice
     * @return double
     */
    protected function calculateDiscount($salePrice, $listPrice)
    {
        if ('double' != gettype($salePrice) && 'integer' != gettype($salePrice)) {
            throw new \InvalidArgumentException('Invalid type of sale price argument value "'.gettype($salePrice).'"');
        }
        if ('double' != gettype($listPrice) && 'integer' != gettype($listPrice)) {
            throw new \InvalidArgumentException('Invalid type of list price argument value "'.gettype($listPrice).'"');
        }
        
        return $listPrice / $salePrice - 1.0;
    }
    
}
