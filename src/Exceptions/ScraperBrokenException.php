<?php

namespace App\Exceptions;

/**
 * Either the website has changed or the website started using ShapeSecurity.com technologies
 * or just a bug in the scraper code :)
 */
class ScraperBrokenException extends \Exception {}
