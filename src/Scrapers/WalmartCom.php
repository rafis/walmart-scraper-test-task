<?php

namespace App\Scrapers;

use App\Arr;
use App\ProductScraper;
use App\Exceptions\ScraperBrokenException;

/**
 * Walmart.com is a dynamic website and is not functional without JavaScript.
 * To scrape data we must parse Redux Initial State from main html and pickup options from
 * https://www.walmart.com/terra-firma/item/<product>/location/<zipcode>?selected=true&wl13= .
 */
class WalmartCom extends ProductScraper
{
    /**
     * Product fields which are available to be scraped
     */
    const PRODUCT_FIELDS = [
        'title',
        'variants', // 1.а. Найти наличие на складе каждого варианта
        'offers', // 1.b. Найти цену для каждого варианта
        'shipping:zipcode' // 2. Найти все варианты (название, стоимость) доставки данного товара по индексу 10001
    ];
    
    protected $_jsonData;
    
    /**
     * Get current product JSON data
     * 
     * @return array
     */
    protected function getReduxInitialState()
    {
        if (null !== $this->_jsonData) {
            return $this->_jsonData;
        }
        
        $crawler = $this->_client->request('GET', $this->_pathname);
        
        $jsonDataPattern = '/__WML_REDUX_INITIAL_STATE__\s*=\s*(.+?)\s*;\s*\<\s*\/script\s*\>/';
        
        if ( ! preg_match($jsonDataPattern, $this->_client->getResponse()->getContent(), $matchesJsonData)) {
            throw new ScraperBrokenException('Could not find Redux initial state');
        }
        
        $this->_jsonData = json_decode($matchesJsonData[1], true);
        
        return $this->_jsonData;
    }

    /**
     * Get primary product ID
     * 
     * @return string
     * @throws ScraperBrokenException
     */
    protected function getPrimaryProductId()
    {
        $jsonData = $this->getReduxInitialState();
        
        $primaryProductId = Arr::get($jsonData, 'product.primaryProduct');
        if ('' == trim($primaryProductId)) {
            throw new ScraperBrokenException('Could not get primary product ID from JSON data');
        }
        
        return $primaryProductId;
    }
    
    /**
     * Get primary product
     * 
     * @return array
     * @throws ScraperBrokenException
     */
    protected function getPrimaryProduct()
    {
        $jsonData = $this->getReduxInitialState();
        
        $primaryProductId = $this->getPrimaryProductId();
        
        $primaryProduct = Arr::get($jsonData, "product.products.$primaryProductId");
        if ( ! $primaryProduct) {
            throw new ScraperBrokenException('Could not get primary product from JSON data');
        }
        
        return $primaryProduct;
    }
    
    public function scrapeTitle()
    {
        $primaryProduct = $this->getPrimaryProduct();
        
        $title = Arr::get($primaryProduct, 'productAttributes.productName');
        
        return $title;
    }
    
    public function scrapeVariants()
    {
        $jsonData = $this->getReduxInitialState();
        
        $primaryProductId = $this->getPrimaryProductId();
        
        $variants = [];

        $productVariants = Arr::get($jsonData, "product.variantCategoriesMap.$primaryProductId", []);

        foreach($productVariants as $productAttribute => $productAttributeData) {

            foreach($productAttributeData['variants'] as $productAttributeVariantId => $productAttributeVariant) {

                $variant = [];
                $variant['id'] = $productAttributeVariant['id'];
                $variant['attributeName'] = $productAttributeData['name'];
                $variant['title'] = $productAttributeData['name'] . ' ' . $productAttributeVariant['name'];
                $variant['fullTitle'] = $this->scrapeTitle() . ' - ' . $variant['title'];
                if ('AVAILABLE' == $productAttributeVariant['availabilityStatus']) {
                    $variantAvailability = self::VARIANT_STATUS_AVAILABLE;
                } else if ('NOT_AVAILABLE' == $productAttributeVariant['availabilityStatus']) {
                    $variantAvailability = self::VARIANT_STATUS_NOT_AVAILABLE;
                } else {
                    throw new ScraperBrokenException('Could not get primary product from JSON data');
                }
                $variant['availability'] = $variantAvailability;

                $variants[] = $variant;

            }

        }
        
        return $variants;
    }
    
    public function scrapeOffers()
    {
        $jsonData = $this->getReduxInitialState();

        $primaryProductId = $this->getPrimaryProductId();
        
        $offers = [];

        // First prepare helper variants array
        $variants = [];

        $productVariants = Arr::get($jsonData, "product.variantCategoriesMap.$primaryProductId", []);

        foreach($productVariants as $productAttribute => $productAttributeData) {

            foreach($productAttributeData['variants'] as $productAttributeVariantId => $productAttributeVariant) {

                $variant = [];

                $variant['title'] = $productAttributeData['name'] . ' ' . $productAttributeVariant['name'];

                $variants[$productAttributeVariantId] = $variant;

            }

        }

        // Second prepare products array
        $products = [];

        $productProducts = Arr::get($jsonData, "product.products");

        foreach($productProducts as $productProductId => $productProduct) {

            $product = [];

            $productVariant = [];
            $productProductVariants = $productProduct['variants'];
            sort($productProductVariants);
            foreach($productProductVariants as $productAttribute => $productProductVariant) {
                $productVariant[] = $variants[$productProductVariant]['title'];
            }

            $product['variantTitle'] = implode(' - ', $productVariant);
            $product['offers'] = $productProduct['offers'];

            $products[$productProductId] = $product;

        }

        // Now generate offers
        $productOffers = Arr::get($jsonData, "product.offers", []);

        foreach($products as $productProductId => $product) {

            foreach($product['offers'] as $productOfferId) {

                $productOffer = Arr::get($jsonData, "product.offers.$productOfferId");

                $offer = [];

                $offer['fullTitle'] = $this->scrapeTitle() . ' - ' . $product['variantTitle'];
                $offer['price'] = $this->convertToUSD($productOffer['pricesInfo']['priceMap']['CURRENT']['price'], $productOffer['pricesInfo']['priceMap']['CURRENT']['currencyUnit']);
                if (isset($productOffer['pricesInfo']['priceMap']['LIST'])) {
                    $listPrice = $this->convertToUSD($productOffer['pricesInfo']['priceMap']['LIST']['price'], $productOffer['pricesInfo']['priceMap']['LIST']['currencyUnit']);
                    $offer['discount'] = $this->calculateDiscount($offer['price'], $listPrice);
                }
                
                switch($productOffer['productAvailability']['availabilityStatus']) {
                    case 'IN_STOCK':
                        $offerAvailability = self::OFFER_STATUS_IN_STOCK;
                        break;
                    case 'OUT_OF_STOCK':
                        $offerAvailability = self::OFFER_STATUS_OUT_OF_STOCK;
                        break;
                    default:
                        throw new ScraperBrokenException('Unsupported product availability status "'.$productOffer['productAvailability']['availabilityStatus'].'"');
                }
                $offer['availability'] = $offerAvailability;

                $offers[] = $offer;

            }

        }
        
        return $offers;
    }
    
    public function scrapeShipping($zipcode)
    {
        if ( ! preg_match('/^\d{5}(?:[-\s]\d{4})?$/', $zipcode)) {
            throw new \InvalidArgumentException('Invalid zip code "'.$zipcode.'"');
        }
        
        $shippingOptions = [];
        
        $primaryProductId = $this->getPrimaryProductId();
        
        $crawler = $this->_client->request('GET', '/terra-firma/item/'.$primaryProductId.'/location/'.$zipcode.'?selected=true&wl13=');
        
        if ( ! preg_match('/^application\/json(?:;\s*charset\=utf\-8)?$/i', $this->_client->getResponse()->getHeader('Content-Type'))) {
            throw new ScraperBrokenException('Invalid response from Walmart.com when checking pickup options, Content-Type: "'.$this->_client->getResponse()->getHeader('Content-Type').'"');
        }
        
        $responseData = json_decode($this->_client->getResponse()->getContent(), true);
        $deliveryData = $responseData['payload'];
        
        $productOffersDelivery = Arr::get($deliveryData, 'offers', []);
        
        foreach($productOffersDelivery as $productOfferId => $productOfferDelivery) {
            
            $productShippingOptions = Arr::get($productOfferDelivery, 'fulfillment.shippingOptions', []);
            
            foreach($productShippingOptions as $productShippingOption) {
                
                $shippingOption = [];
                
                $shippingOption['name'] = $productShippingOption['shipMethod'];
                $shippingOption['price'] = $this->convertToUSD($productShippingOption['fulfillmentPrice']['price'], $productShippingOption['fulfillmentPrice']['currencyUnit']);
                if (isset($productShippingOption['fulfillmentDateRange']['exactDeliveryDate'])) {
                    $shippingOption['exactDeliveryDate'] = date('Y-m-d H:i:s', $productShippingOption['fulfillmentDateRange']['exactDeliveryDate'] / 1000);
                } else if (isset($productShippingOption['fulfillmentDateRange']['earliestDeliverDate'])) {
                    $shippingOption['earliestDeliverDate'] = date('Y-m-d H:i:s', $productShippingOption['fulfillmentDateRange']['earliestDeliverDate'] / 1000);
                    $shippingOption['latestDeliveryDate'] = date('Y-m-d H:i:s', $productShippingOption['fulfillmentDateRange']['latestDeliveryDate'] / 1000);
                } else {
                    throw new ScraperBrokenException('Unknown fulfillment date range '.json_encode($productShippingOption['fulfillmentDateRange']));
                }
                
                $shippingOptions[] = $shippingOption;
                
            }
            
        }
        
        return $shippingOptions;
    }
    
}
