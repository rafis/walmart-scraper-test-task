<?php

namespace App;

use App\GoutteClient;
use App\ScrapingHelper;

abstract class ProductScraper
{
    /**
     * Product variant status of availability
     */
    const VARIANT_STATUS_NOT_AVAILABLE = 0;
    const VARIANT_STATUS_AVAILABLE = 1;

    /**
     * Product offer status of availability
     */
    const OFFER_STATUS_OUT_OF_STOCK = 0;
    const OFFER_STATUS_IN_STOCK = 1;
    const OFFER_STATUS_TO_ORDER = 2;
    
    /**
     * Inject helper functions
     */
    use ScrapingHelper;

    /**
     * HTTP request sender
     *
     * @var GoutteClient 
     */
    protected $_client;
    
    /**
     * Pathname pointing to concrete product page
     * 
     * @var string 
     */
    protected $_pathname;
    
    /**
     * Supported product fields for scraping
     * 
     * @return array
     */
    public static function getFields() {
        
        return static::PRODUCT_FIELDS;
        
    }
    
    /**
     * Constructor
     */
    public function __construct($pathname)
    {
        $this->_pathname = $pathname;
        
        $options = [
            'base_uri' => 'https://www.walmart.com/',
            'verify' => false,
        ];
        
        $client = new GoutteClient([
            'HTTP_USER_AGENT' => 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36',
            'HTTP_ACCEPT' => 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'HTTP_ACCEPT_LANGUAGE' => 'en-US,en;q=0.5',
            'HTTP_ACCEPT_ENCODING' => 'gzip, deflate',
        ]);
        
        // Guzzle will determine an appropriate default handler if `null` is given.
        $defaultHandler = null;

        // Basic directory cache example
        $cacheProvider = new \Doctrine\Common\Cache\FilesystemCache(realpath(__DIR__.'/../cache'));

        // Create a cache handler with a given cache provider and default handler.
        $options['handler'] = new \Concat\Http\Handler\CacheHandler($cacheProvider, $defaultHandler, [

            /**
             * @var array HTTP methods that should be cached.
             */
            'methods' => ['GET', 'HEAD', 'OPTIONS'],

            /**
             * @var integer Time in seconds to cache a response for.
             */
            'expire' => 7200,

            /**
             * @var callable Accepts a request and returns true if it should be cached.
             */
            'filter' => null,
        ]);

        // Use a PSR-3 compliant logger to log when bundles are stored or fetched.
        //$handler->setLogger($logger);

        // Merge options
        $options = $options + [
            'allow_redirects' => false,
            'cookies' => true,
            'handler' => isset($options['handler']) ? $options['handler'] : null,
        ];
        
        // Create a Guzzle 6 client, passing the cache handler as 'handler'.
        $guzzle = new \GuzzleHttp\Client($options);
        $client->setClient($guzzle);
        
        $this->_client = $client;
    }
    
    /**
     * Scrape specific field of the current product
     * 
     * @param string $field
     * @return mixed
     */
    public function scrape($field)
    {
        
        if (false !== strpos($field, ':')) {
            $arguments = explode(',', substr($field, strpos($field, ':') + 1));
            $field = substr($field, 0, strpos($field, ':'));
        } else {
            $arguments = [];
        }
        
        return call_user_func_array([$this, 'scrape'.ucfirst($field)], $arguments);
        
    }
    
}
