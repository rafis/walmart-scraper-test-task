<?php

namespace App;

use Goutte\Client;

/**
 * Modified Goutte\Client with cookies bug fixed
 */
class GoutteClient extends Client
{

    /**
     * Calls a URI.
     *
     * @param string $method        The request method
     * @param string $uri           The URI to fetch
     * @param array  $parameters    The Request parameters
     * @param array  $files         The files
     * @param array  $server        The server parameters (HTTP headers are referenced with a HTTP_ prefix as PHP does)
     * @param string $content       The raw body data
     * @param bool   $changeHistory Whether to update the history or not (only used internally for back(), forward(), and reload())
     *
     * @return Crawler
     */
    public function request($method, $uri, array $parameters = array(), array $files = array(), array $server = array(), $content = null, $changeHistory = true)
    {
        if (preg_match('/^\/[^\/]?/', $uri)) {
            $uri = rtrim($this->getClient()->getConfig('base_uri'), '/') . $uri;
        }
        
        // Fix Goutte's cookies
        $headerCookie = [];
        $cookieUrl = $this->getHistory()->isEmpty() ? $this->getClient()->getConfig('base_uri') : $this->getHistory()->current()->getUri();
        $cookieUrl = parse_url($cookieUrl);
        $cookieUrl = $cookieUrl['scheme'] . '://' . $cookieUrl['host'] . '/';

        $parts = array_replace(['path' => '/'], parse_url($cookieUrl));
        $cookies = [];
        foreach ($this->getCookieJar()->all() as $cookie) {
            $domain = $cookie->getDomain();
            if ($domain) {
                $domain = '.'.ltrim($domain, '.');
                if ($domain != substr('.'.$parts['host'], -strlen($domain))) {
                    continue;
                }
            }

            $path = $cookie->getPath();
            if (0 !== strpos($path, $parts['path'])) {
                continue;
            }

            if ($cookie->isSecure() && 'https' != $parts['scheme']) {
                continue;
            }

            $cookies[$cookie->getName()] = $cookie->getValue();
        } unset($cookie);
        unset($parts);
        unset($cookieUrl);

        if (count($cookies)) {
            foreach($cookies as $key => $value) {
                $headerCookie[] = urlencode($key).'='.urlencode(rtrim($value, ';'));
            } unset($key); unset($value);
            unset($cookies);

            $headerCookie = implode("; ", $headerCookie);
            $server['HTTP_COOKIE'] = (isset($server['HTTP_COOKIE']) ? $server['HTTP_COOKIE'].'; ' : '').$headerCookie;

            unset($headerCookie);
        }
        
        return parent::request($method, $uri, $parameters, $files, $server, $content, $changeHistory);
    }
    
}
